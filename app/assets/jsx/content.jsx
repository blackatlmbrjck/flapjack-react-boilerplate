var DefaultProps = React.createClass({
  getDefaultProps: function(){
    return {
      txt: 'this is a default property',
      cat: 0
    }
  },
  propTypes: {
    txt: React.PropTypes.string,
    cat: React.PropTypes.number.isRequired
  },
  render: function() {
    var txt = this.props.txt
    return (
      <div className="well">
        <h3>Default Props</h3>
        <p>{txt}</p>
      </div>
    );
  }
});

var CommentList = React.createClass({
  render: function() {
    return (
      <div className="box-one well">
        <h3>Box 1</h3>
      </div>
    );
  }
});

var CommentForm = React.createClass({
  render: function() {
    return (
      <div className="box-two well">
        <h3>Box two</h3>
      </div>
    );
  }
});

var App = React.createClass({
  render: function() {
    return (
      <div className="box-wrapper">
        <div className="page-header">
          <h1>Box Wrapper</h1>
        </div>
        <CommentList />
        <CommentForm />
        <DefaultProps />
      </div>
    );
  }
});

React.render(<App />, document.getElementById('app'));